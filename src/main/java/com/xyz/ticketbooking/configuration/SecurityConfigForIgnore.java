package com.xyz.ticketbooking.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Profile("test")
@Configuration
public class SecurityConfigForIgnore extends WebSecurityConfigurerAdapter {

    @Override
    public void configure(WebSecurity security) throws Exception {
        security.ignoring().antMatchers("/**");
    }
}