package com.xyz.ticketbooking.controller;

import com.xyz.ticketbooking.dto.ErrorDto;
import com.xyz.ticketbooking.exception.InvalidRequestException;
import com.xyz.ticketbooking.exception.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(DataIntegrityViolationException.class)
    public void handleConflict(DataIntegrityViolationException exp) {
        log.debug(exp.getMessage());
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<ErrorDto> handleBindingException(MethodArgumentTypeMismatchException exp) {
        log.debug(exp.getMessage());
        return new ResponseEntity<>(new ErrorDto("Argument Type Error",
                "Wrong Argument for " + exp.getName() ), HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(InvalidDataAccessApiUsageException.class)
    public ResponseEntity<ErrorDto> handleInvalidDataAccessException(InvalidDataAccessApiUsageException exp) {
        log.debug(exp.getMessage());
        return new ResponseEntity<>(new ErrorDto(exp.getMessage(), ""), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorDto> handleValidationException(MethodArgumentNotValidException exp) {
        log.debug(exp.getMessage());
        BindingResult result = exp.getBindingResult();
        List<FieldError> fieldErrors = result.getFieldErrors();
        String errorMsg = fieldErrors.stream().map((fieldError) ->{
            return fieldError.getField() + ":"+ fieldError.getDefaultMessage();
        }).collect(Collectors.joining(","));
        for (org.springframework.validation.FieldError fieldError: fieldErrors) {
            errorMsg.concat(fieldError.getField() + ":"+ fieldError.getDefaultMessage());
        }
        return new ResponseEntity<>(new ErrorDto("Validation Error", errorMsg), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(EmptyResultDataAccessException.class)
    public ResponseEntity<ErrorDto> handleEmptyResultException(EmptyResultDataAccessException exp) {
        log.debug(exp.getMessage());
        return new ResponseEntity<>(new ErrorDto("Resource Not Found Error", exp.getMessage()),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InvalidRequestException.class)
    public ResponseEntity<ErrorDto> handleInvalidRequestException(InvalidRequestException exp) {
        log.debug(exp.getMessage());
        return new ResponseEntity<>(new ErrorDto("Invalid Request ", exp.getMessage()),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ErrorDto> handleNotFoundException(NotFoundException exp) {
        log.debug(exp.getMessage());
        return new ResponseEntity<>(new ErrorDto(exp.getMessage(), ""), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(Throwable.class)
    public ResponseEntity<ErrorDto>  handleOtherException(Throwable exp) {
        exp.printStackTrace();
        log.debug(exp.getMessage());
        return new ResponseEntity<ErrorDto> (new ErrorDto("UnExpected Internal Error", ""),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

}