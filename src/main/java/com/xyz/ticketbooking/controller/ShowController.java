package com.xyz.ticketbooking.controller;

import com.xyz.ticketbooking.dto.MovieShow;
import com.xyz.ticketbooking.exception.InvalidRequestException;
import com.xyz.ticketbooking.exception.NotFoundException;
import com.xyz.ticketbooking.service.ShowService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.time.LocalDate;
import java.util.UUID;

@RestController
@Slf4j
public class ShowController {

    private ShowService showService;

    public ShowController(ShowService showService) {
        this.showService = showService;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/show")
    @ResponseBody
    public Iterable<MovieShow> getShows(HttpServletRequest req,
                                        @RequestParam(name = "theatreId", required = false) UUID theatreId,
                                        @RequestParam(name = "movieId", required = false) UUID movieId,
                                        @RequestParam(name = "showDate", required = false)
                                            @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate showDate)  {

        Iterable<MovieShow> shows = this.showService.getShows(theatreId, showDate, movieId);
        if (!shows.iterator().hasNext()) throw new NotFoundException("Show not found for the Criteria");
        return shows;
    }

    @RequestMapping(method = RequestMethod.POST, path = "/show")
    @ResponseBody
    public MovieShow addTheatre(HttpServletRequest req,
                           @Valid @RequestBody MovieShow show) {
        return this.showService.addShow(show);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/show")
    @ResponseBody
    public void updateTheatre(HttpServletRequest req,
                              @RequestBody MovieShow show){
        if (null == show.getShowId()) throw new InvalidRequestException("showId should not be null");
        this.showService.updateShow(show);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/show/{showId}")
    @ResponseBody
    public void deleteTheatre(HttpServletRequest req,
                              @PathVariable("showId") UUID showId) {
        this.showService.deleteShow(showId);
    }
}
