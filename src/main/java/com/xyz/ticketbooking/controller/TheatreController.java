package com.xyz.ticketbooking.controller;

import com.xyz.ticketbooking.dto.Theatre;
import com.xyz.ticketbooking.exception.InvalidRequestException;
import com.xyz.ticketbooking.exception.NotFoundException;
import com.xyz.ticketbooking.service.TheatreService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@RestController
@Slf4j
public class TheatreController {
    private TheatreService theatreService;

    public TheatreController(TheatreService theatreService) {
        this.theatreService = theatreService;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/theatre")
    @ResponseBody
    public Iterable<Theatre> getTheatres(HttpServletRequest req,
                                            @RequestParam(name = "movieId", required = false) UUID movieId,
                                            @RequestParam(name = "city", required = false) String city,
                                            @RequestParam(name = "showDate", required = false) @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate showDate)  {
        List<Theatre> theatres = this.theatreService.getTheatres(movieId, city, showDate);
        if (theatres.isEmpty()) throw new NotFoundException("Threatre not found for the Criteria");
        return theatres;
    }

    @RequestMapping(method = RequestMethod.POST, path = "/theatre")
    @ResponseBody
    public Theatre addTheatre(HttpServletRequest req,
                                            @Valid @RequestBody Theatre theatre) {
            return  this.theatreService.addTheatre(theatre);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/theatre")
    @ResponseBody
    public void updateTheatre(HttpServletRequest req,
                                            @RequestBody Theatre theatre) {
        if (null == theatre.getTheatreId()) throw new InvalidRequestException("TheatreId should not be null");
         this.theatreService.updateTheatre(theatre);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/theatre/{theatreId}")
    @ResponseBody
    public void deleteTheatre(HttpServletRequest req,
                                            @PathVariable("theatreId") UUID theatreId) {
         this.theatreService.deleteTheatre(theatreId);
    }
}
