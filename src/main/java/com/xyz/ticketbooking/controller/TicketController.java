package com.xyz.ticketbooking.controller;

import com.xyz.ticketbooking.dto.Ticket;
import com.xyz.ticketbooking.exception.InvalidRequestException;
import com.xyz.ticketbooking.exception.NotFoundException;
import com.xyz.ticketbooking.service.TicketService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Locale;
import java.util.UUID;

@RestController
@Slf4j
public class TicketController {

    private TicketService ticketService;

    public TicketController(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/ticket")
    @ResponseBody
    public Iterable<Ticket> getTickets(HttpServletRequest req, Locale locale,
                                       @RequestParam(name = "showId", required = false) UUID showId)  {
        log.error("locale : "+locale.getDisplayLanguage());
        log.error("locale : "+locale.getDisplayCountry());
        Iterable<Ticket> tickets = this.ticketService.getTickets(showId);
        if (!tickets.iterator().hasNext()) throw new NotFoundException("Ticket not found for the Criteria");
        return tickets;
    }

    @RequestMapping(method = RequestMethod.POST, path = "/ticket")
    @ResponseBody
    public Ticket addTheatre(HttpServletRequest req,
                             @Valid @RequestBody Ticket ticket) {
        return this.ticketService.addTicket(ticket);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/ticket")
    @ResponseBody
    public void updateTicket(HttpServletRequest req,
                              @RequestBody Ticket ticket){
        if (null == ticket.getTicketId()) throw new InvalidRequestException("ticketId should not be null");
        this.ticketService.updateTicket(ticket);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/ticket/{ticketId}")
    @ResponseBody
    public void deleteTicket(HttpServletRequest req,
                              @PathVariable("ticketId") UUID ticketId) {
        this.ticketService.deleteTicket(ticketId);
    }
}

