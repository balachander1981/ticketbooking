package com.xyz.ticketbooking.data;

import com.xyz.ticketbooking.dto.Seat;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface SeatRepository extends CrudRepository<Seat, UUID>, JpaSpecificationExecutor<Seat> {
}