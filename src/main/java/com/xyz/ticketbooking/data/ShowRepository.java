package com.xyz.ticketbooking.data;

import com.xyz.ticketbooking.dto.MovieShow;
import com.xyz.ticketbooking.dto.Theatre;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface ShowRepository extends CrudRepository<MovieShow, UUID>, JpaSpecificationExecutor<MovieShow> {
    @Override
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    Optional<MovieShow> findById(UUID uuid);
}
