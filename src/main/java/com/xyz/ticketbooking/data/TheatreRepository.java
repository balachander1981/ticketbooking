package com.xyz.ticketbooking.data;

import com.xyz.ticketbooking.dto.Theatre;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface TheatreRepository extends CrudRepository<Theatre, UUID>, JpaSpecificationExecutor<Theatre> {
    @Override
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    Optional<Theatre> findById(UUID uuid);
}
