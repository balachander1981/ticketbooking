package com.xyz.ticketbooking.data.spec;

import com.xyz.ticketbooking.dto.MovieShow;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Slf4j
public class ShowSpecification {
    public static Specification<MovieShow> getShowCriteria(UUID theatreId, LocalDate showDate, UUID movieId) {
        return (showRoot, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();

            if (null != theatreId) {
                log.debug("Add theatreId Criteria");
                predicates.add(criteriaBuilder.equal(
                        criteriaBuilder.upper(showRoot.get("theatre").get("theatreId")), theatreId));
            }

            if (null != movieId) {
                log.debug("Add movieId Criteria");
                predicates.add(criteriaBuilder.equal(
                        criteriaBuilder.upper(showRoot.get("movie").get("movieId")), movieId));
            }
            if (null != showDate) {
                log.debug("Add showDate Criteria");
                predicates.add(criteriaBuilder.between(
                        showRoot.get("showTime"),
                        showDate.atStartOfDay(), showDate.plusDays(1).atStartOfDay()));
            }
            //to handle duplicates in join result
            criteriaQuery.distinct(true);
            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}