package com.xyz.ticketbooking.data.spec;

import com.xyz.ticketbooking.dto.Theatre;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
@Slf4j
public class TheatreSpecification {
    public static Specification<Theatre> getTheatreCriteria(UUID movieId, String city, LocalDate showDate) {
        return (theatreRoot, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();

            if (null != movieId) {
                // to handle n+1 queries
                theatreRoot.fetch("shows", JoinType.INNER);
                log.debug("Add movieId Criteria");
                predicates.add(criteriaBuilder.equal(
                        theatreRoot.join("shows", JoinType.INNER).get("movie").get("movieId"), movieId));
            }
            if (null != city) {
                log.debug("Add city Criteria");
                predicates.add(criteriaBuilder.equal(
                        criteriaBuilder.upper(theatreRoot.get("theatreCity")), city.trim().toUpperCase()));
            }
            if (null != showDate) {
                log.debug("Add showDate Criteria");
                theatreRoot.fetch("shows", JoinType.INNER);
                predicates.add(criteriaBuilder.between(
                        theatreRoot.join("shows", JoinType.INNER).get("showTime"),
                        showDate.atStartOfDay(), showDate.plusDays(1).atStartOfDay()));
            }
            //to handle duplicates in join result
            criteriaQuery.distinct(true);
            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}