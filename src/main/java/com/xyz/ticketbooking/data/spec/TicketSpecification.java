package com.xyz.ticketbooking.data.spec;

import com.xyz.ticketbooking.dto.Ticket;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
@Slf4j
public class TicketSpecification {
    public static Specification<Ticket> getShowCriteria(UUID showId) {
        return (showRoot, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();

            if (null != showId) {
                log.debug("Add showId Criteria");
                predicates.add(criteriaBuilder.equal(
                        criteriaBuilder.upper(showRoot.get("show").get("showId")), showId));
            }
            //to handle duplicates in join result
            criteriaQuery.distinct(true);
            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}