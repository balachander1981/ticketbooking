package com.xyz.ticketbooking.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class ErrorDto {

    private String msg;
    private String detail;

    @JsonCreator
    public ErrorDto(@JsonProperty("msg") String msg, @JsonProperty("detail")String detail) {
        this.msg = msg;
        this.detail = detail;
    }
}
