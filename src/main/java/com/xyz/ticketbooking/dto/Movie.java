package com.xyz.ticketbooking.dto;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.Duration;
import java.util.UUID;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Table(name = "movie")
public class Movie {
    @Id
    @GeneratedValue
    private UUID movieId;
    @NotBlank
    private String movieName;
    @NotBlank
    private String moviePlot;
    @NotBlank
    private String movieCast;
    @NotNull
    private Duration movieDuration;

    public Movie(String movieName, String moviePlot, String movieCast, Duration movieDuration) {
        this.movieName = movieName;
        this.moviePlot = moviePlot;
        this.movieCast = movieCast;
        this.movieDuration = movieDuration;
    }
}
