package com.xyz.ticketbooking.dto;

import com.fasterxml.jackson.annotation.*;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table(name = "movie_show")
public class MovieShow {
    @Id
    @GeneratedValue
    private UUID showId;
    @NotNull
    @ManyToOne
    @JoinColumn(name = "movie_id")
    private Movie movie;
    @NotNull
    @ManyToOne
    @JoinColumn(name = "theatre_id")
    @JsonProperty(access= JsonProperty.Access.WRITE_ONLY)
    private Theatre theatre;
    @NotNull
    private LocalDateTime showTime;

    public MovieShow(Movie movie, Theatre theatre, LocalDateTime showTime) {
        this.movie = movie;
        this.theatre = theatre;
        this.showTime = showTime;
    }
}
