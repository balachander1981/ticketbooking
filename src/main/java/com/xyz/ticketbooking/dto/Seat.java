package com.xyz.ticketbooking.dto;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Table(name = "seat")
public class Seat {
    @Id
    @GeneratedValue
    private UUID seatId;
    @NotNull
    @ManyToOne
    @JoinColumn(name = "theatre_id")
    private Theatre theatre;
    @NotNull
    private String seatRow;
    @NotNull
    private Integer seatNo;

    public Seat(Theatre theatre, String seatRow, Integer seatNo) {
        this.theatre = theatre;
        this.seatRow = seatRow;
        this.seatNo = seatNo;
    }
}
