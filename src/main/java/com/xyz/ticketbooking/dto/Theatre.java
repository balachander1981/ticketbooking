package com.xyz.ticketbooking.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table(name = "theatre")
public class Theatre {
    @Id
    @GeneratedValue
    private UUID theatreId;
    @OneToMany(mappedBy="theatre", cascade = CascadeType.ALL)
    private Set<MovieShow> shows;
    @NotBlank
    private String theatreName;
    @NotBlank
    private String theatreCity;
    @NotBlank
    private String theatreAddress;
    @Min(value = 0)
    private Integer totalSeats;

    public Theatre(Set<MovieShow> shows, String theatreName, String theatreCity, String theatreAddress, Integer totalSeats) {
        this.shows = shows;
        this.theatreName = theatreName;
        this.theatreCity = theatreCity;
        this.theatreAddress = theatreAddress;
        this.totalSeats = totalSeats;
    }
}
