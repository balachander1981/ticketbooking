package com.xyz.ticketbooking.dto;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Table(name = "ticket")
public class Ticket {
    @Id
    @GeneratedValue
    private UUID ticketId;
    @NotNull
    @ManyToOne
    @JoinColumn(name = "show_id")
    private MovieShow show;
    @NotNull
    @ManyToOne
    @JoinColumn(name = "seat_id")
    private Seat seat;

    public Ticket(MovieShow show, Seat seat) {
        this.show = show;
        this.seat = seat;
    }
}
