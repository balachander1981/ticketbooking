package com.xyz.ticketbooking.service;

import com.xyz.ticketbooking.data.ShowRepository;
import com.xyz.ticketbooking.data.spec.ShowSpecification;
import com.xyz.ticketbooking.dto.MovieShow;
import com.xyz.ticketbooking.exception.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.stereotype.Service;

import javax.persistence.LockModeType;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.Optional;
import java.util.UUID;

@Service
@Slf4j
public class ShowService {
    private ShowRepository showRepository;

    public ShowService(ShowRepository showRepository) {
        this.showRepository = showRepository;
    }

    public Iterable<MovieShow> getShows(UUID theatreId, LocalDate showDate, UUID movieId) {
        return this.showRepository.findAll(ShowSpecification.getShowCriteria(theatreId, showDate, movieId));
    }

    @Transactional
    public MovieShow addShow(MovieShow show) {
        return this.showRepository.save(show);
    }

    @Transactional
    public void updateShow(MovieShow show) {
        Optional<MovieShow> movieShow = this.showRepository.findById(show.getShowId());
        if (movieShow.isPresent())
         this.showRepository.save(show);
        else
            throw new NotFoundException("MovieShow: " +show.getShowId() +" Not found");
    }

    @Transactional
    public void deleteShow(UUID showId) {
         this.showRepository.deleteById(showId);
    }
}
