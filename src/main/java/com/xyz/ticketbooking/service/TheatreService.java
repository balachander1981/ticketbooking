package com.xyz.ticketbooking.service;

import com.xyz.ticketbooking.data.TheatreRepository;
import com.xyz.ticketbooking.data.spec.TheatreSpecification;
import com.xyz.ticketbooking.dto.MovieShow;
import com.xyz.ticketbooking.dto.Theatre;
import com.xyz.ticketbooking.exception.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.stereotype.Service;

import javax.persistence.LockModeType;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Slf4j
public class TheatreService {
    private TheatreRepository theatreRepository;

    public TheatreService(TheatreRepository theatreRepository) {
        this.theatreRepository = theatreRepository;
    }

    public List<Theatre> getTheatres(UUID movieId, String city, LocalDate showDate) {
        List<Theatre> theatres = this.theatreRepository.findAll(
                TheatreSpecification.getTheatreCriteria(movieId, city, showDate));
        if (null != movieId) {
            log.debug("Filter Threatre based on movieId");
            theatres.forEach((theatre) -> {
                Set<MovieShow> filteredShows = theatre.getShows().stream().filter((show) -> {
                    return show.getMovie().getMovieId().equals(movieId);
                }).collect(Collectors.toSet());
                theatre.setShows(filteredShows);
            });
        }
        if (null != showDate) {
            log.debug("Filter Threatre.Shows based on showDate");
            theatres.forEach((theatre) -> {
                Set<MovieShow> filteredShows = theatre.getShows().stream().filter((show) -> {
                    return show.getShowTime().isAfter(showDate.atStartOfDay()) &&
                            show.getShowTime().isBefore(showDate.plusDays(1).atStartOfDay());
                }).collect(Collectors.toSet());
                theatre.setShows(filteredShows);
            });
        }
        return theatres;
    }

    @Transactional
    public Theatre addTheatre(Theatre theatre) {
        return this.theatreRepository.save(theatre);
    }

    @Transactional
    public void updateTheatre(Theatre theatre) {
        Optional<Theatre> storedThreatre = this.theatreRepository.findById(theatre.getTheatreId());
        if (storedThreatre.isPresent())
            this.theatreRepository.save(theatre);
        else
            throw new NotFoundException("Theatre: " +theatre.getTheatreId() +" Not found");
    }

    @Transactional
    public void deleteTheatre(UUID theatreId) {
        this.theatreRepository.deleteById(theatreId);
    }
}
