package com.xyz.ticketbooking.service;

import com.xyz.ticketbooking.data.TicketRepository;
import com.xyz.ticketbooking.data.spec.TicketSpecification;
import com.xyz.ticketbooking.dto.Ticket;
import com.xyz.ticketbooking.exception.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.stereotype.Service;

import javax.persistence.LockModeType;
import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;

@Service
@Slf4j
public class TicketService {

    private TicketRepository ticketRepository;

    public TicketService(TicketRepository ticketRepository) {
        this.ticketRepository = ticketRepository;
    }

    public Iterable<Ticket> getTickets(UUID showId) {
        return this.ticketRepository.findAll(
                TicketSpecification.getShowCriteria(showId));
    }
    @Transactional
    public Ticket addTicket(Ticket ticket) {
        return this.ticketRepository.save(ticket);
    }

    @Transactional
    public void updateTicket(Ticket ticket) {
        Optional<Ticket> storedTicket = this.ticketRepository.findById(ticket.getTicketId());
        if (storedTicket.isPresent())
            this.ticketRepository.save(ticket);
        else
            throw new NotFoundException("Ticket: " +ticket.getTicketId() +" Not found");
        this.ticketRepository.save(ticket);
    }

    @Transactional
    public void deleteTicket(UUID ticketId) {
        this.ticketRepository.deleteById(ticketId);
    }
}
