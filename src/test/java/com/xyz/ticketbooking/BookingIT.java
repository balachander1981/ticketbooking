package com.xyz.ticketbooking;

import com.xyz.ticketbooking.data.*;
import com.xyz.ticketbooking.dto.*;
import org.apache.lucene.util.fst.FST;
import org.assertj.core.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.server.LocalServerPort;

import java.time.Duration;
import java.time.LocalDate;
import java.util.UUID;

public class BookingIT {

    @Autowired
    public TicketRepository ticketRepository;
    @Autowired
    public SeatRepository seatRepository;
    @Autowired
    public TheatreRepository theatreRepository;
    @Autowired
    public MovieRepository movieRepository;
    @Autowired
    protected ShowRepository showRepository;

    @LocalServerPort
    private int port;

    protected Movie[] movies = new Movie[3];
    protected Theatre[] theatres = new Theatre[3];
    protected MovieShow[] shows = new MovieShow[8];
    protected Seat[] seats = new Seat[10];
    protected Ticket[] tickets = new Ticket[10];

    @Value("${server.servlet.context-path}")
    private String contextRoot;

    protected String createURLWithPort(String uri) {
        uri = this.contextRoot + uri;
        return "http://localhost:" + port + uri;
    }

    protected void setUpTheatresAndShows() {
        setUpTheatres();
        shows[0] = this.showRepository.save(new MovieShow(movies[0], theatres[0], LocalDate.now().atStartOfDay()));
        shows[1] = this.showRepository.save(new MovieShow(movies[1], theatres[0], LocalDate.now().atStartOfDay().plusHours(3)));
        shows[2] = this.showRepository.save(new MovieShow(movies[1], theatres[0], LocalDate.now().atStartOfDay().plusHours(6)));
        shows[3] = this.showRepository.save(new MovieShow(movies[0], theatres[1], LocalDate.now().atStartOfDay()));
        shows[4] = this.showRepository.save(new MovieShow(movies[2], theatres[1], LocalDate.now().atStartOfDay().plusHours(3)));
        shows[5] = this.showRepository.save(new MovieShow(movies[1], theatres[1], LocalDate.now().atStartOfDay().plusHours(26)));
        shows[6] = this.showRepository.save(new MovieShow(movies[0], theatres[2], LocalDate.now().atStartOfDay()));
        shows[7] = this.showRepository.save(new MovieShow(movies[2], theatres[2], LocalDate.now().atStartOfDay().plusHours(26)));

    }

    protected void setUpTheatres() {
        theatres[0] = theatreRepository.save(new Theatre(null, "theatre1", "city1",
                "address1", 100));
        theatres[1] = theatreRepository.save(new Theatre(null, "theatre2", "city1",
                "address2", 50));
        theatres[2] = theatreRepository.save(new Theatre(null, "theatre3", "city3",
                "address3", 75));
    }

    protected void setupMovies() {
        movies = Arrays.array(new Movie( "movie1", "qwerty asdfgh", "a,s,d,f,g", Duration.ofMinutes(120)),
                new Movie( "movie2", "qwerty asdfgh", "a,s,d,f,g", Duration.ofMinutes(120)),
                new Movie("movie3", "qwerty asdfgh", "a,s,d,f,g", Duration.ofMinutes(120)));
        movieRepository.save(movies[0]);
        movieRepository.save(movies[1]);
        movieRepository.save(movies[2]);
    }

    protected void setUpTickets() {
        tickets[0] = ticketRepository.save(new Ticket(shows[0], seats[0]));
        tickets[1] = ticketRepository.save(new Ticket(shows[0], seats[1]));
        tickets[2] = ticketRepository.save(new Ticket(shows[0], seats[2]));
        tickets[3] = ticketRepository.save(new Ticket(shows[0], seats[3]));
        tickets[4] = ticketRepository.save(new Ticket(shows[0], seats[4]));
        tickets[5] = ticketRepository.save(new Ticket(shows[1], seats[0]));
        tickets[6] = ticketRepository.save(new Ticket(shows[1], seats[1]));
        tickets[7] = ticketRepository.save(new Ticket(shows[1], seats[2]));
        tickets[8] = ticketRepository.save(new Ticket(shows[1], seats[3]));
        tickets[9] = ticketRepository.save(new Ticket(shows[1], seats[4]));
    }

    protected void setupSeats() {
        seats[0] = seatRepository.save(new Seat( theatres[0], "A", 1));
        seats[1] = seatRepository.save(new Seat( theatres[0], "A", 2));
        seats[2] = seatRepository.save(new Seat( theatres[0], "A", 3));
        seats[3] = seatRepository.save(new Seat( theatres[0], "A", 4));
        seats[4] = seatRepository.save(new Seat( theatres[0], "A", 5));
        seats[5] = seatRepository.save(new Seat( theatres[0], "B", 1));
        seats[6] = seatRepository.save(new Seat( theatres[0], "B", 2));
        seats[7] = seatRepository.save(new Seat( theatres[0], "B", 3));
        seats[8] = seatRepository.save(new Seat( theatres[0], "B", 4));
        seats[9] = seatRepository.save(new Seat( theatres[0], "B", 5));
    }
}
