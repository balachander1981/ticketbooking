package com.xyz.ticketbooking;

import com.xyz.ticketbooking.data.ShowRepository;
import com.xyz.ticketbooking.dto.ErrorDto;
import com.xyz.ticketbooking.dto.MovieShow;
import com.xyz.ticketbooking.dto.MovieShowForTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class ShowIT extends BookingIT {

    public static final String CURRENT_DATE = LocalDate.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
    TestRestTemplate restTemplate = new TestRestTemplate();
    HttpHeaders headers = new HttpHeaders();
    @Autowired
    public ShowRepository showRepository;

    @Test
    public void fetchShows() throws Exception {
        setupMovies();
        setUpTheatresAndShows();
        Iterable<MovieShow> all = this.showRepository.findAll();
        HttpEntity<String> getEntity = new HttpEntity<String>(headers);
        ResponseEntity<List<MovieShow>> showResponse = restTemplate.exchange(
                createURLWithPort("/show"), HttpMethod.GET,
                getEntity, new ParameterizedTypeReference<List<MovieShow>>() {
                });

        assertEquals(HttpStatus.OK, showResponse.getStatusCode());
        assertEquals(8, showResponse.getBody().size());
    }

    @Test
    public void fetchShowsByTheatreId() throws Exception {
        setupMovies();
        setUpTheatresAndShows();
        HttpEntity<String> getEntity = new HttpEntity<String>(headers);
        ResponseEntity<List<MovieShow>> showResponse = restTemplate.exchange(
                createURLWithPort("/show?theatreId=" + theatres[0].getTheatreId()), HttpMethod.GET,
                getEntity, new ParameterizedTypeReference<List<MovieShow>>() {
                });

        assertEquals(HttpStatus.OK, showResponse.getStatusCode());
        assertEquals(3, showResponse.getBody().size());
    }

    @Test
    public void fetchShowsbByMovieId() throws Exception {
        setupMovies();
        setUpTheatresAndShows();
        HttpEntity<String> getEntity = new HttpEntity<String>(headers);
        ResponseEntity<List<MovieShow>> showResponse = restTemplate.exchange(
                createURLWithPort("/show?movieId=" + movies[2].getMovieId()), HttpMethod.GET,
                getEntity, new ParameterizedTypeReference<List<MovieShow>>() {
                });

        assertEquals(HttpStatus.OK, showResponse.getStatusCode());
        assertEquals(2, showResponse.getBody().size());
    }

    @Test
    public void fetchShowsbByShowDate() throws Exception {
        setupMovies();
        setUpTheatresAndShows();
        HttpEntity<String> getEntity = new HttpEntity<String>(headers);
        ResponseEntity<List<MovieShow>> showResponse = restTemplate.exchange(
                createURLWithPort("/show?showDate=" + CURRENT_DATE), HttpMethod.GET,
                getEntity, new ParameterizedTypeReference<List<MovieShow>>() {
                });

        assertEquals(HttpStatus.OK, showResponse.getStatusCode());
        assertEquals(6, showResponse.getBody().size());
    }

    @Test
    public void fetchShowsByMovieIdAndDate() throws Exception {

        setupMovies();
        setUpTheatresAndShows();

        HttpEntity<String> getEntity = new HttpEntity<String>(headers);
        ResponseEntity<List<MovieShow>> showResponse = restTemplate.exchange(
                createURLWithPort("/show?movieId=" + movies[1].getMovieId() + "&showDate=" + CURRENT_DATE), HttpMethod.GET,
                getEntity, new ParameterizedTypeReference<List<MovieShow>>() {
                });

        assertEquals(HttpStatus.OK, showResponse.getStatusCode());
        assertEquals(2, showResponse.getBody().size());

    }

    @Test
    public void fetchShowsByWrongMovieIdShouldReturn404() throws Exception {

        setupMovies();
        setUpTheatresAndShows();

        HttpEntity<String> getEntity = new HttpEntity<String>(headers);
        ResponseEntity<ErrorDto> showResponse = restTemplate.exchange(
                createURLWithPort("/show?movieId=" + UUID.randomUUID()), HttpMethod.GET,
                getEntity, ErrorDto.class);

        assertEquals(HttpStatus.NOT_FOUND, showResponse.getStatusCode());
        assertEquals("Show not found for the Criteria", showResponse.getBody().getMsg());
    }

    @Test
    public void fetchShowsByInvalidParamsTypeShouldThrow400() throws Exception {

        setupMovies();
        setUpTheatresAndShows();

        HttpEntity<String> getEntity = new HttpEntity<String>(headers);
        ResponseEntity<ErrorDto> showResponse = restTemplate.exchange(
                createURLWithPort("/show?movieId=wwe"), HttpMethod.GET,
                getEntity, ErrorDto.class);

        assertEquals(HttpStatus.BAD_REQUEST, showResponse.getStatusCode());
        assertEquals("Argument Type Error", showResponse.getBody().getMsg());
        assertEquals("Wrong Argument for movieId", showResponse.getBody().getDetail());
    }

    @Test
    public void addShowsAndFetch() throws Exception {
        setupMovies();
        setUpTheatres();
        HttpEntity<MovieShowForTest> show1 = new HttpEntity<MovieShowForTest>(new MovieShowForTest(movies[0], theatres[0], LocalDate.now().atStartOfDay()), headers);
        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort("/show"), HttpMethod.POST,
                show1, String.class);

        HttpEntity<MovieShowForTest> show2 = new HttpEntity<MovieShowForTest>(new MovieShowForTest(movies[0], theatres[0], LocalDate.now().atStartOfDay()), headers);
        restTemplate.exchange(
                createURLWithPort("/show"), HttpMethod.POST,
                show2, String.class);

        HttpEntity<MovieShowForTest> show3 = new HttpEntity<MovieShowForTest>(new MovieShowForTest(movies[0], theatres[0], LocalDate.now().atStartOfDay()), headers);
        restTemplate.exchange(
                createURLWithPort("/show"), HttpMethod.POST,
                show3, String.class);


        HttpEntity<String> getEntity = new HttpEntity<String>(headers);
        ResponseEntity<List<MovieShow>> showResponse = restTemplate.exchange(
                createURLWithPort("/show"), HttpMethod.GET,
                getEntity, new ParameterizedTypeReference<List<MovieShow>>() {
                });

        assertEquals(HttpStatus.OK, showResponse.getStatusCode());
        assertEquals(3, showResponse.getBody().size());
    }

    @Test
    public void addShowsWithInvalidValuesShouldReturn400() throws Exception {
        setupMovies();
        setUpTheatres();
        HttpEntity<MovieShowForTest> show1 = new HttpEntity<MovieShowForTest>(new MovieShowForTest(null, theatres[0], LocalDate.now().atStartOfDay()), headers);
        ResponseEntity<ErrorDto> showResponse = restTemplate.exchange(
                createURLWithPort("/show"), HttpMethod.POST,
                show1, ErrorDto.class);

        assertEquals(HttpStatus.BAD_REQUEST, showResponse.getStatusCode());
        assertEquals("Validation Error", showResponse.getBody().getMsg());
        assertEquals("movie:must not be null", showResponse.getBody().getDetail());

    }

    @Test
    public void updateShowsAndFetch() throws Exception {
        setupMovies();
        setUpTheatres();
        HttpEntity<MovieShowForTest> show1 = new HttpEntity<MovieShowForTest>(new MovieShowForTest(movies[0], theatres[0], LocalDate.now().atStartOfDay()), headers);
        ResponseEntity<MovieShow> response = restTemplate.exchange(
                createURLWithPort("/show"), HttpMethod.POST,
                show1, MovieShow.class);

        HttpEntity<MovieShowForTest> updatedShow = new HttpEntity<MovieShowForTest>(new MovieShowForTest(
                response.getBody().getShowId(), movies[1], theatres[0], LocalDate.now().atStartOfDay()), headers);
        ResponseEntity<String> updateResponse = restTemplate.exchange(
                createURLWithPort("/show"), HttpMethod.PUT,
                updatedShow, String.class);
        assertEquals(HttpStatus.OK, updateResponse.getStatusCode());


        HttpEntity<String> getEntity = new HttpEntity<String>(headers);
        ResponseEntity<List<MovieShow>> showResponse = restTemplate.exchange(
                createURLWithPort("/show"), HttpMethod.GET,
                getEntity, new ParameterizedTypeReference<List<MovieShow>>() {
                });

        assertEquals(HttpStatus.OK, showResponse.getStatusCode());
        assertEquals(1, showResponse.getBody().size());
        assertEquals(movies[1], showResponse.getBody().get(0).getMovie());
    }

    @Test
    public void updateShowsWithNullShowIdReturns400() throws Exception {
        setupMovies();
        setUpTheatres();
        HttpEntity<MovieShowForTest> show1 = new HttpEntity<MovieShowForTest>(new MovieShowForTest(movies[0], theatres[0], LocalDate.now().atStartOfDay()), headers);
        ResponseEntity<MovieShow> response = restTemplate.exchange(
                createURLWithPort("/show"), HttpMethod.POST,
                show1, MovieShow.class);

        HttpEntity<MovieShowForTest> updatedShow = new HttpEntity<MovieShowForTest>(new MovieShowForTest(
                null, movies[1], theatres[0], LocalDate.now().atStartOfDay()), headers);
        ResponseEntity<String> updateResponse = restTemplate.exchange(
                createURLWithPort("/show"), HttpMethod.PUT,
                updatedShow, String.class);
        assertEquals(HttpStatus.BAD_REQUEST, updateResponse.getStatusCode());

    }


    @Test
    public void deleteShow() throws Exception {

        setupMovies();
        setUpTheatresAndShows();

        HttpEntity<String> entity = new HttpEntity<String>(headers);
        ResponseEntity<List<MovieShow>> showResponse = restTemplate.exchange(
                createURLWithPort("/show?movieId=" + movies[0].getMovieId()), HttpMethod.GET,
                entity, new ParameterizedTypeReference<List<MovieShow>>() {
                });

        assertEquals(HttpStatus.OK, showResponse.getStatusCode());
        assertEquals(3, showResponse.getBody().size());
        UUID showId = showResponse.getBody().get(0).getShowId();
        ResponseEntity<String> showDelResponse = restTemplate.exchange(
                createURLWithPort("/show/" + showId), HttpMethod.DELETE, entity, String.class);
        assertEquals(HttpStatus.OK, showDelResponse.getStatusCode());

        showResponse = restTemplate.exchange(
                createURLWithPort("/show?movieId=" + movies[0].getMovieId()), HttpMethod.GET,
                entity, new ParameterizedTypeReference<List<MovieShow>>() {
                });

        assertEquals(HttpStatus.OK, showResponse.getStatusCode());
        assertEquals(2, showResponse.getBody().size());

    }

    @Test
    public void deleteTheatreWithInvalidIdShowReturn400() throws Exception {

        setupMovies();
        setUpTheatresAndShows();

        HttpEntity<String> entity = new HttpEntity<String>(headers);
        UUID uuid = UUID.randomUUID();
        ResponseEntity<ErrorDto> showDelResponse = restTemplate.exchange(
                createURLWithPort("/show/" + uuid), HttpMethod.DELETE, entity, ErrorDto.class);
        assertEquals(HttpStatus.BAD_REQUEST, showDelResponse.getStatusCode());
        assertEquals("Resource Not Found Error", showDelResponse.getBody().getMsg());
        assertEquals("No class com.xyz.ticketbooking.dto.MovieShow entity with id " + uuid + " exists!", showDelResponse.getBody().getDetail());

    }

}