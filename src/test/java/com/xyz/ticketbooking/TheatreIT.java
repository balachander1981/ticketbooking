package com.xyz.ticketbooking;


import com.xyz.ticketbooking.data.MovieRepository;
import com.xyz.ticketbooking.data.TheatreRepository;
import com.xyz.ticketbooking.dto.ErrorDto;
import com.xyz.ticketbooking.dto.MovieShow;
import com.xyz.ticketbooking.dto.Theatre;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class TheatreIT extends BookingIT {

    TestRestTemplate restTemplate = new TestRestTemplate();
    HttpHeaders headers = new HttpHeaders();
    @Autowired
    public TheatreRepository theatreRepository;
    @Autowired
    public MovieRepository movieRepository;

    @Test
    public void fetchTheatres() throws Exception {
        setupMovies();
        setUpTheatresAndShows();
        HttpEntity<String> getEntity = new HttpEntity<String>(headers);
        ResponseEntity<List<Theatre>> showResponse = restTemplate.exchange(
                createURLWithPort("/theatre"), HttpMethod.GET,
                getEntity, new ParameterizedTypeReference<List<Theatre>>() {
                });

        assertEquals(HttpStatus.OK, showResponse.getStatusCode());
        assertEquals(3, showResponse.getBody().size());
    }

    @Test
    public void fetchTheatresByMovieId() throws Exception {

        setupMovies();
        setUpTheatresAndShows();

        HttpEntity<String> getEntity = new HttpEntity<String>(headers);
        ResponseEntity<List<Theatre>> showResponse = restTemplate.exchange(
                createURLWithPort("/theatre?movieId="+movies[1].getMovieId()), HttpMethod.GET,
                getEntity, new ParameterizedTypeReference<List<Theatre>>() {
                });

        assertEquals(HttpStatus.OK, showResponse.getStatusCode());
        assertEquals(2, showResponse.getBody().size());
    }

    @Test
    public void fetchTheatresByWrongMovieIdShouldReturn404() throws Exception {

        setupMovies();
        setUpTheatresAndShows();

        HttpEntity<String> getEntity = new HttpEntity<String>(headers);
        ResponseEntity<ErrorDto> showResponse = restTemplate.exchange(
                createURLWithPort("/theatre?movieId="+UUID.randomUUID()), HttpMethod.GET,
                getEntity, ErrorDto.class);

        assertEquals(HttpStatus.NOT_FOUND, showResponse.getStatusCode());
        assertEquals("Threatre not found for the Criteria", showResponse.getBody().getMsg());
    }

    @Test
    public void fetchTheatresByInvalidParamsTypeShouldThrow400() throws Exception {

        setupMovies();
        setUpTheatresAndShows();

        HttpEntity<String> getEntity = new HttpEntity<String>(headers);
        ResponseEntity<ErrorDto> showResponse = restTemplate.exchange(
                createURLWithPort("/theatre?movieId=wwe"), HttpMethod.GET,
                getEntity, ErrorDto.class);

        assertEquals(HttpStatus.BAD_REQUEST, showResponse.getStatusCode());
        assertEquals("Argument Type Error", showResponse.getBody().getMsg());
        assertEquals("Wrong Argument for movieId", showResponse.getBody().getDetail());
    }

    @Test
    public void fetchTheatresByCity() throws Exception {

        setupMovies();
        setUpTheatresAndShows();

        HttpEntity<String> getEntity = new HttpEntity<String>(headers);
        ResponseEntity<List<Theatre>> showResponse = restTemplate.exchange(
                createURLWithPort("/theatre?city=city1"), HttpMethod.GET,
                getEntity, new ParameterizedTypeReference<List<Theatre>>() {
                });

        assertEquals(HttpStatus.OK, showResponse.getStatusCode());
        assertEquals(2, showResponse.getBody().size());

    }

    @Test
    public void fetchTheatresByMovieIdAndCity() throws Exception {

        setupMovies();
        setUpTheatresAndShows();

        HttpEntity<String> getEntity = new HttpEntity<String>(headers);
        ResponseEntity<List<Theatre>> showResponse = restTemplate.exchange(
                createURLWithPort("/theatre?movieId="+movies[1].getMovieId()+"&city=city1"), HttpMethod.GET,
                getEntity, new ParameterizedTypeReference<List<Theatre>>() {
                });

        assertEquals(HttpStatus.OK, showResponse.getStatusCode());
        assertEquals(2, showResponse.getBody().size());

    }

    @Test
    public void fetchTheatresByDate() throws Exception {

        setupMovies();
        setUpTheatresAndShows();

        HttpEntity<String> getEntity = new HttpEntity<String>(headers);
        String currentDate = LocalDate.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        ResponseEntity<List<Theatre>> showResponse = restTemplate.exchange(
                createURLWithPort("/theatre?showDate="+currentDate), HttpMethod.GET,
                getEntity, new ParameterizedTypeReference<List<Theatre>>() {
                });

        assertEquals(HttpStatus.OK, showResponse.getStatusCode());
        assertEquals(3, showResponse.getBody().size());

    }

    @Test
    public void addTheatres() throws Exception {
        setupMovies();
         HttpEntity<Theatre> theatre = new HttpEntity<Theatre>(new Theatre( null, "theatre1", "city1",
                "address1", 100), headers);
        ResponseEntity<String> showResponse = restTemplate.exchange(
                createURLWithPort("/theatre"), HttpMethod.POST,
                theatre, String.class);

        assertEquals(HttpStatus.OK, showResponse.getStatusCode());

        HttpEntity<String> getEntity = new HttpEntity<String>(headers);
        String currentDate = LocalDate.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        ResponseEntity<List<Theatre>> theatreGetResponse = restTemplate.exchange(
                createURLWithPort("/theatre"), HttpMethod.GET,
                getEntity, new ParameterizedTypeReference<List<Theatre>>() {
                });

        assertEquals(HttpStatus.OK, theatreGetResponse.getStatusCode());
        assertEquals(1, theatreGetResponse.getBody().size());
    }

    @Test
    public void addTheatresWithInvalidValuesShouldReturn400() throws Exception {
         HttpEntity<Theatre> theatre = new HttpEntity<Theatre>(new Theatre( null, null, "city1",
                "address1", 100), headers);
        ResponseEntity<ErrorDto> showResponse = restTemplate.exchange(
                createURLWithPort("/theatre"), HttpMethod.POST,
                theatre, ErrorDto.class);

        assertEquals(HttpStatus.BAD_REQUEST, showResponse.getStatusCode());
        assertEquals("Validation Error", showResponse.getBody().getMsg());
        assertEquals("theatreName:must not be blank", showResponse.getBody().getDetail());

    }

    @Test
    public void addTheatresAndFetch() throws Exception {

        HttpEntity<Theatre> theatre1 = new HttpEntity<Theatre>(new Theatre( null, "theatre1", "city1",
                "address1", 100), headers);
        restTemplate.exchange(
                createURLWithPort("/theatre"), HttpMethod.POST,
                theatre1, String.class);

        HttpEntity<Theatre> theatre2 = new HttpEntity<Theatre>(new Theatre( null, "theatre2", "city2",
                "address2", 100), headers);
        restTemplate.exchange(
                createURLWithPort("/theatre"), HttpMethod.POST,
                theatre2, String.class);

        HttpEntity<Theatre> theatre3 = new HttpEntity<Theatre>(new Theatre(null, "theatre3", "city3",
                "address3", 100), headers);
        restTemplate.exchange(
                createURLWithPort("/theatre"), HttpMethod.POST,
                theatre3, String.class);

        HttpEntity<String> getEntity = new HttpEntity<String>(headers);
        ResponseEntity<List<Theatre>> showResponse = restTemplate.exchange(
                createURLWithPort("/theatre"), HttpMethod.GET,
                getEntity, new ParameterizedTypeReference<List<Theatre>>() {
                });

        assertEquals(HttpStatus.OK, showResponse.getStatusCode());
        assertEquals(3, showResponse.getBody().size());
    }

    @Test
    public void updateTheatresAndFetch() throws Exception {
        setupMovies();

        HttpEntity<Theatre> theatre2 = new HttpEntity<Theatre>(new Theatre( null, "theatre2", "city2",
                "address2", 100), headers);
        ResponseEntity<Theatre> response = restTemplate.exchange(
                createURLWithPort("/theatre"), HttpMethod.POST,
                theatre2, Theatre.class);
        Theatre theatre = response.getBody();

         theatre2 = new HttpEntity<Theatre>(new Theatre(theatre.getTheatreId(), null, "newtheatre2", "city2",
                "address2", 100), headers);
        ResponseEntity<String> showResponse = restTemplate.exchange(
                createURLWithPort("/theatre"), HttpMethod.PUT,
                theatre2, String.class);

        assertEquals(HttpStatus.OK, showResponse.getStatusCode());

        HttpEntity<String> getEntity = new HttpEntity<String>(headers);
        ResponseEntity<List<Theatre>> theatreGetResponse = restTemplate.exchange(
                createURLWithPort("/theatre"), HttpMethod.GET,
                getEntity, new ParameterizedTypeReference<List<Theatre>>() {
                });

        assertEquals(HttpStatus.OK, theatreGetResponse.getStatusCode());
        assertEquals(1, theatreGetResponse.getBody().size());
        assertEquals("newtheatre2", theatreGetResponse.getBody().get(0).getTheatreName());
    }

    @Test
    public void updateTheatresWithNullTheareIdReturns400() throws Exception {
        setupMovies();

        HttpEntity<Theatre> theatre2 = new HttpEntity<Theatre>(new Theatre( null, "theatre2", "city2",
                "address2", 100), headers);
        ResponseEntity<Theatre> response = restTemplate.exchange(
                createURLWithPort("/theatre"), HttpMethod.POST,
                theatre2, Theatre.class);
        Theatre theatre = response.getBody();

         theatre2 = new HttpEntity<Theatre>(new Theatre(null, null, "newtheatre2", "city2",
                "address2", 100), headers);
        ResponseEntity<String> showResponse = restTemplate.exchange(
                createURLWithPort("/theatre"), HttpMethod.PUT,
                theatre2, String.class);

        assertEquals(HttpStatus.BAD_REQUEST, showResponse.getStatusCode());

    }

    @Test
    public void deleteTheatre() throws Exception {

        setupMovies();
        setUpTheatresAndShows();

        HttpEntity<String> entity = new HttpEntity<String>(headers);
        ResponseEntity<List<Theatre>> showResponse = restTemplate.exchange(
                createURLWithPort("/theatre?movieId=" + movies[1].getMovieId()), HttpMethod.GET,
                entity,  new ParameterizedTypeReference<List<Theatre>>() {
                });

        UUID theatreId = showResponse.getBody().get(0).getTheatreId();
        assertEquals(HttpStatus.OK, showResponse.getStatusCode());
        assertEquals(2, showResponse.getBody().size());

        ResponseEntity<String> theatreDelResponse = restTemplate.exchange(
                createURLWithPort("/theatre/"+theatres[0].getTheatreId()), HttpMethod.DELETE, entity, String.class);
        assertEquals(HttpStatus.OK, theatreDelResponse.getStatusCode());

         showResponse = restTemplate.exchange(
                createURLWithPort("/theatre?movieId=" + movies[1].getMovieId()), HttpMethod.GET,
                entity, new ParameterizedTypeReference<List<Theatre>>() {
                });

        assertEquals(HttpStatus.OK, showResponse.getStatusCode());
        assertEquals(1, showResponse.getBody().size());

    }

    @Test
    public void deleteTheatreWithInvalidIdShowReturn400() throws Exception {

        setupMovies();
        setUpTheatresAndShows();

        HttpEntity<String> entity = new HttpEntity<String>(headers);

        UUID uuid = UUID.randomUUID();
        ResponseEntity<ErrorDto> theatreDelResponse = restTemplate.exchange(
                createURLWithPort("/theatre/"+ uuid), HttpMethod.DELETE, entity, ErrorDto.class);
        assertEquals(HttpStatus.BAD_REQUEST, theatreDelResponse.getStatusCode());
        assertEquals("Resource Not Found Error", theatreDelResponse.getBody().getMsg());
        assertEquals("No class com.xyz.ticketbooking.dto.Theatre entity with id "+ uuid+" exists!", theatreDelResponse.getBody().getDetail());

    }

}