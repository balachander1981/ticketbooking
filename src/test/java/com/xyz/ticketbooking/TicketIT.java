package com.xyz.ticketbooking;


import com.xyz.ticketbooking.dto.*;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class TicketIT extends BookingIT {

    TestRestTemplate restTemplate = new TestRestTemplate();
    HttpHeaders headers = new HttpHeaders();

    @Test
    public void fetchTickets() throws Exception {
        setupMovies();
        setUpTheatresAndShows();
        setupSeats();
        setUpTickets();
        HttpEntity<String> getEntity = new HttpEntity<String>(headers);
        ResponseEntity<List<Ticket>> ticketResponse = restTemplate.exchange(
                createURLWithPort("/ticket"), HttpMethod.GET,
                getEntity, new ParameterizedTypeReference<List<Ticket>>() {
                });

        assertEquals(HttpStatus.OK, ticketResponse.getStatusCode());
        assertEquals(10, ticketResponse.getBody().size());
    }



    @Test
    public void fetchTicketsByWrongMovieIdShouldReturn404() throws Exception {
        setupMovies();
        setUpTheatresAndShows();
        setupSeats();
        setUpTickets();

        HttpEntity<String> getEntity = new HttpEntity<String>(headers);
        ResponseEntity<ErrorDto> ticketResponse = restTemplate.exchange(
                createURLWithPort("/ticket?showId="+ UUID.randomUUID()), HttpMethod.GET,
                getEntity, ErrorDto.class);

        assertEquals(HttpStatus.NOT_FOUND, ticketResponse.getStatusCode());
        assertEquals("Ticket not found for the Criteria", ticketResponse.getBody().getMsg());
    }

    @Test
    public void fetchTicketsByInvalidParamsTypeShouldThrow400() throws Exception {
        setupMovies();
        setUpTheatresAndShows();
        setupSeats();
        setUpTickets();

        HttpEntity<String> getEntity = new HttpEntity<String>(headers);
        ResponseEntity<ErrorDto> ticketResponse = restTemplate.exchange(
                createURLWithPort("/ticket?showId=wwe"), HttpMethod.GET,
                getEntity, ErrorDto.class);

        assertEquals(HttpStatus.BAD_REQUEST, ticketResponse.getStatusCode());
        assertEquals("Argument Type Error", ticketResponse.getBody().getMsg());
        assertEquals("Wrong Argument for showId", ticketResponse.getBody().getDetail());
    }


    @Test
    public void addTickets() throws Exception {
        setupMovies();
        setUpTheatresAndShows();
        setupSeats();
        HttpEntity<Ticket> ticket = new HttpEntity<Ticket>(new Ticket(shows[0], seats[2]), headers);
        ResponseEntity<String> ticketResponse = restTemplate.exchange(
                createURLWithPort("/ticket"), HttpMethod.POST,
                ticket, String.class);

        assertEquals(HttpStatus.OK, ticketResponse.getStatusCode());

        HttpEntity<String> getEntity = new HttpEntity<String>(headers);
        String currentDate = LocalDate.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        ResponseEntity<List<Ticket>> ticketGetResponse = restTemplate.exchange(
                createURLWithPort("/ticket?showId="+shows[0].getShowId()), HttpMethod.GET,
                getEntity, new ParameterizedTypeReference<List<Ticket>>() {
                });

        assertEquals(HttpStatus.OK, ticketGetResponse.getStatusCode());
        assertEquals(1, ticketGetResponse.getBody().size());
    }

    @Test
    public void addTicketsWithInvalidValuesShouldReturn400() throws Exception {
        setupMovies();
        setUpTheatresAndShows();
        setupSeats();
        HttpEntity<Ticket> ticket = new HttpEntity<Ticket>(new Ticket(shows[0], null), headers);
        ResponseEntity<ErrorDto> ticketResponse = restTemplate.exchange(
                createURLWithPort("/ticket"), HttpMethod.POST,
                ticket, ErrorDto.class);

        assertEquals(HttpStatus.BAD_REQUEST, ticketResponse.getStatusCode());
        assertEquals("Validation Error", ticketResponse.getBody().getMsg());
        assertEquals("seat:must not be null", ticketResponse.getBody().getDetail());

    }

    @Test
    public void addTicketsAndFetch() throws Exception {
        setupMovies();
        setUpTheatresAndShows();
        setupSeats();
        HttpEntity<Ticket> ticket1 = new HttpEntity<Ticket>(new Ticket(shows[0], seats[0]), headers);
        restTemplate.exchange(
                createURLWithPort("/ticket"), HttpMethod.POST,
                ticket1, String.class);

        HttpEntity<Ticket> ticket2 = new HttpEntity<Ticket>(new Ticket(shows[0], seats[1]), headers);
        restTemplate.exchange(
                createURLWithPort("/ticket"), HttpMethod.POST,
                ticket2, String.class);

        HttpEntity<Ticket> ticket3 = new HttpEntity<Ticket>(new Ticket(shows[0], seats[2]), headers);
        restTemplate.exchange(
                createURLWithPort("/ticket"), HttpMethod.POST,
                ticket3, String.class);

        HttpEntity<String> getEntity = new HttpEntity<String>(headers);
        ResponseEntity<List<Ticket>> ticketResponse = restTemplate.exchange(
                createURLWithPort("/ticket"), HttpMethod.GET,
                getEntity, new ParameterizedTypeReference<List<Ticket>>() {
                });

        assertEquals(HttpStatus.OK, ticketResponse.getStatusCode());
        assertEquals(3, ticketResponse.getBody().size());
    }

    @Test
    public void updateTicketsAndFetch() throws Exception {
        setupMovies();
        setUpTheatresAndShows();
        setupSeats();
        HttpEntity<Ticket> ticket1 = new HttpEntity<Ticket>(new Ticket( shows[0], seats[1]), headers);
        Ticket ticket = restTemplate.exchange(
                createURLWithPort("/ticket"), HttpMethod.POST,
                ticket1, Ticket.class).getBody();

        HttpEntity<Ticket> ticket2 = new HttpEntity<Ticket>(new Ticket(ticket.getTicketId(), shows[0], seats[2]), headers);
        ResponseEntity<String> ticketResponse = restTemplate.exchange(
                createURLWithPort("/ticket"), HttpMethod.PUT,
                ticket2, String.class);

        assertEquals(HttpStatus.OK, ticketResponse.getStatusCode());

        HttpEntity<String> getEntity = new HttpEntity<String>(headers);
        ResponseEntity<List<Ticket>> ticketGetResponse = restTemplate.exchange(
                createURLWithPort("/ticket"), HttpMethod.GET,
                getEntity, new ParameterizedTypeReference<List<Ticket>>() {
                });

        assertEquals(HttpStatus.OK, ticketGetResponse.getStatusCode());
        assertEquals(1, ticketGetResponse.getBody().size());
        assertEquals(seats[2].getSeatId(), ticketGetResponse.getBody().get(0).getSeat().getSeatId());
    }

    @Test
    public void updateTicketswithNullTicketIdReturns400() throws Exception {
        setupMovies();
        setUpTheatresAndShows();
        setupSeats();
        HttpEntity<Ticket> ticket1 = new HttpEntity<Ticket>(new Ticket( shows[0], seats[1]), headers);
        Ticket ticket = restTemplate.exchange(
                createURLWithPort("/ticket"), HttpMethod.POST,
                ticket1, Ticket.class).getBody();

        HttpEntity<Ticket> ticket2 = new HttpEntity<Ticket>(new Ticket(null, shows[0], seats[2]), headers);
        ResponseEntity<String> ticketResponse = restTemplate.exchange(
                createURLWithPort("/ticket"), HttpMethod.PUT,
                ticket2, String.class);

        assertEquals(HttpStatus.BAD_REQUEST, ticketResponse.getStatusCode());

    }

    @Test
    public void deleteTicket() throws Exception {
        setupMovies();
        setUpTheatresAndShows();
        setupSeats();
        setUpTickets();

        HttpEntity<String> entity = new HttpEntity<String>(headers);
        ResponseEntity<List<Ticket>> ticketResponse = restTemplate.exchange(
                createURLWithPort("/ticket?showId="+shows[1].getShowId()), HttpMethod.GET,
                entity, new ParameterizedTypeReference<List<Ticket>>() {
                });

        assertEquals(HttpStatus.OK, ticketResponse.getStatusCode());
        assertEquals(5, ticketResponse.getBody().size());

        ResponseEntity<String> ticketDelResponse = restTemplate.exchange(
                createURLWithPort("/ticket/"+tickets[5].getTicketId()), HttpMethod.DELETE, entity, String.class);
        assertEquals(HttpStatus.OK, ticketDelResponse.getStatusCode());

        ticketResponse = restTemplate.exchange(
                createURLWithPort("/ticket?showId="+shows[1].getShowId()), HttpMethod.GET,
                entity, new ParameterizedTypeReference<List<Ticket>>() {
                });

        assertEquals(HttpStatus.OK, ticketResponse.getStatusCode());
        assertEquals(4, ticketResponse.getBody().size());

    }

    @Test
    public void deleteTicketWithInvalidIdShowReturn400() throws Exception {
        setupMovies();
        setUpTheatresAndShows();
        setupSeats();
        setUpTickets();

        HttpEntity<String> entity = new HttpEntity<String>(headers);

        UUID uuid = UUID.randomUUID();
        ResponseEntity<ErrorDto> ticketDelResponse = restTemplate.exchange(
                createURLWithPort("/ticket/"+ uuid), HttpMethod.DELETE, entity, ErrorDto.class);
        assertEquals(HttpStatus.BAD_REQUEST, ticketDelResponse.getStatusCode());
        assertEquals("Resource Not Found Error", ticketDelResponse.getBody().getMsg());
        assertEquals("No class com.xyz.ticketbooking.dto.Ticket entity with id "+ uuid +" exists!", ticketDelResponse.getBody().getDetail());

    }

}