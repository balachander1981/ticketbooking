package com.xyz.ticketbooking.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xyz.ticketbooking.dto.Movie;
import com.xyz.ticketbooking.dto.Theatre;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class MovieShowForTest {
    private UUID showId;
    private Movie movie;
    private Theatre theatre;
    private LocalDateTime showTime;

    public MovieShowForTest(Movie movie, Theatre theatre, LocalDateTime showTime) {
        this.movie = movie;
        this.theatre = theatre;
        this.showTime = showTime;
    }
}
